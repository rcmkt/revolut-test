package rcmkt.revoluttest.utils

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class CurrencyFormatterTest {

    @Test
    fun `when format amount with many numbers in decimal part - when return 2 numbers after sign`() {

        //given
        val rawAmount = 400.56789

        //when
        val formattedAmount = CurrencyFormatter.formatToString(rawAmount)

        //then
        assertThat(formattedAmount).isEqualTo("400,56")
    }

    @Test
    fun `when format amount only with integer part - when return without zero decimal part`() {

        //given
        val rawAmount = 400.0

        //when
        val formattedAmount = CurrencyFormatter.formatToString(rawAmount)

        //then
        assertThat(formattedAmount).isEqualTo("400")
    }

    @Test
    fun `when format nullable amount - when return zero with decimal part`() {

        //given
        val rawAmount: Double? = null

        //when
        val formattedAmount = CurrencyFormatter.formatToString(rawAmount)

        //then
        assertThat(formattedAmount).isEqualTo("0,00")
    }

    @Test
    fun `when format zero amount - when return zero with decimal part`() {

        //given
        val rawAmount = 0.0

        //when
        val formattedAmount = CurrencyFormatter.formatToString(rawAmount)

        //then
        assertThat(formattedAmount).isEqualTo("0,00")
    }

    @Test
    fun `when format amount which more or equal than thousand - when return value with space after group numbers`() {

        //given
        val rawAmount = 4000.01
        val groupingSeparator = '\u00A0'

        //when
        val formattedAmount = CurrencyFormatter.formatToString(rawAmount)

        //then
        assertThat(formattedAmount).isEqualTo("4${groupingSeparator}000,01")
    }
}