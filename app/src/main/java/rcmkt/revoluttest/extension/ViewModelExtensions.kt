package rcmkt.revoluttest.extension

import androidx.fragment.app.Fragment
import androidx.lifecycle.*

inline fun <reified T : ViewModel> Fragment.obtainViewModel(viewModelFactory: ViewModelProvider.Factory? = null): T {
    return ViewModelProviders.of(this, viewModelFactory)[T::class.java]
}

inline fun <reified T : Any, reified L : LiveData<T>> LifecycleOwner.observe(liveData: L, noinline block: (T) -> Unit) {
    liveData.observe(this, Observer<T> { it?.let(block::invoke) })
}