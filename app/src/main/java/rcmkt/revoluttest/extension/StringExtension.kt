@file:Suppress("unused")

package rcmkt.revoluttest.extension

val String.Companion.EMPTY: String
    get() = ""

val String.Companion.WHITESPACE: String
    get() = "\u00A0"