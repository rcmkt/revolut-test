package rcmkt.revoluttest.extension

import android.content.Context
import android.os.Handler
import android.view.View
import android.view.inputmethod.InputMethodManager

fun View.showKeyboardWithDelay() {
    Handler()
        .postDelayed({
            val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(this, InputMethodManager.SHOW_IMPLICIT)
        }, 200)
}