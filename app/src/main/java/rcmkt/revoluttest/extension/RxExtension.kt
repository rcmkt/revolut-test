package rcmkt.revoluttest.extension

import io.reactivex.Observable
import rcmkt.domain.internal.SchedulersProvider

fun <T> Observable<T>.schedulersIoToMain(schedulersProvider: SchedulersProvider): Observable<T> {
    return subscribeOn(schedulersProvider.io())
        .observeOn(schedulersProvider.mainThread())
}