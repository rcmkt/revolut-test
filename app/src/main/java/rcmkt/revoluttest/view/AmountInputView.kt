package rcmkt.revoluttest.view

import android.content.Context
import android.util.AttributeSet
import android.widget.EditText
import rcmkt.revoluttest.utils.CurrencyFormatter

class AmountInputView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : EditText(context, attrs, defStyleAttr) {

    private var amountInputListener: AmountInputListener? = null

    override fun onTextChanged(text: CharSequence, start: Int, lengthBefore: Int, lengthAfter: Int) {
        super.onTextChanged(text, start, lengthBefore, lengthAfter)
        if (lengthAfter != lengthBefore) {
            val formattedAmount = getFormattedText(text.toString())
            setFormattedText(formattedAmount)
            setSelection(formattedAmount.length)
        }
    }

    override fun onSelectionChanged(selStart: Int, selEnd: Int) {
        val immutableText = text
        if (!immutableText.isNullOrEmpty() && getAmount() == 0.0) {
            setSelection(immutableText!!.length)
        }
        super.onSelectionChanged(selStart, selEnd)
    }

    fun setAmount(amount: String) {
        val formattedAmount = getFormattedText(amount)
        setFormattedText(formattedAmount)
        setSelection(formattedAmount.length)
    }

    fun getAmount(): Double {
        return text.toString()
            .replace(" ", "")
            .replace(",", ".")
            .toDoubleOrNull() ?: 0.0
    }

    fun setAmountListener(listener: AmountInputListener) {
        amountInputListener = listener
    }

    fun removeAmountListener() {
        amountInputListener = null
    }

    private fun setFormattedText(text: String) {
        setText(text)
        amountInputListener?.onAmountChanged(getAmount())
    }

    private fun getFormattedText(text: String): String {
        return CurrencyFormatter.getSpacedAmount(text)
    }
}