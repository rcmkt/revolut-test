package rcmkt.revoluttest.view

interface AmountInputListener {
    fun onAmountChanged(amount: Double)
}