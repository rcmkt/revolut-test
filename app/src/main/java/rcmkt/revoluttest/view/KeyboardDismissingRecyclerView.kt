package rcmkt.revoluttest.view

import android.content.Context
import android.util.AttributeSet
import android.view.inputmethod.InputMethodManager
import androidx.recyclerview.widget.RecyclerView


class KeyboardHiddingRecyclerView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : RecyclerView(context, attrs, defStyleAttr) {

    private val inputMethodManager: InputMethodManager =
        context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager

    private var scrollListener = object : RecyclerView.OnScrollListener() {
        private var isKeyboardDismissed = false

        override fun onScrollStateChanged(recyclerView: RecyclerView, state: Int) {
            when (state) {
                RecyclerView.SCROLL_STATE_DRAGGING -> if (!isKeyboardDismissed) {
                    hideKeyboard()
                    isKeyboardDismissed = !isKeyboardDismissed
                }
                RecyclerView.SCROLL_STATE_IDLE -> isKeyboardDismissed = false
            }
        }
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        addOnScrollListener(scrollListener)
    }

    override fun onDetachedFromWindow() {
        removeOnScrollListener(scrollListener)
        super.onDetachedFromWindow()
    }

    private fun hideKeyboard() {
        inputMethodManager.hideSoftInputFromWindow(windowToken, 0)
        clearFocus()
    }
}