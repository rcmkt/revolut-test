package rcmkt.revoluttest

import android.app.Application
import rcmkt.revoluttest.di.DI

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        DI.init(this)
    }
}