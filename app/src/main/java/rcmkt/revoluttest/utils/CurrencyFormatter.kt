package rcmkt.revoluttest.utils

import java.math.RoundingMode
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols

object CurrencyFormatter {

    fun formatToString(value: Double?): String {
        return if (value != null && value != 0.0) getFormattedAmount(value) else "0,00"
    }

    fun getSpacedAmount(value: String, maxDecimalLength: Int = 2, maxIntegerLength: Int = 15): String {
        if (value.isEmpty()) return ""

        val parts = getDigitFromString(value).split(',')
        val mainPart = spacedSum(
            if (parts[0].isEmpty()) {
                0
            } else {
                parts[0].substring(0, Math.min(parts[0].length, maxIntegerLength)).toLong()
            }
        )
        val centsPart = if (parts.size > 1 && maxDecimalLength > 0) {
            ",${parts[1].substring(0, Math.min(parts[1].length, maxDecimalLength))}"
        } else {
            ""
        }
        return "$mainPart$centsPart"
    }

    private fun getDigitFromString(string: String): String {
        val withoutDots = string.replace('.', ',')
        var commaFound = false
        return withoutDots.filter {
            val result = !commaFound && it == ',' || it in '0'..'9'
            if (it == ',') commaFound = true
            result
        }
    }

    private fun spacedSum(money: Long): String {
        return money.toString().replace("""(\d)(?=(\d\d\d)+([^\d]|$))""".toRegex(), """$1 """)
    }

    private fun getFormattedAmount(amount: Double): String {
        val symbols = DecimalFormatSymbols().apply {
            groupingSeparator = '\u00A0'
            decimalSeparator = ','
        }
        return DecimalFormat("###,###,##0.##", symbols).apply {
            roundingMode = RoundingMode.DOWN
        }.format(amount)
    }
}