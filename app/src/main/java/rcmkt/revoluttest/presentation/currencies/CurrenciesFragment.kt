package rcmkt.revoluttest.presentation.currencies

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.fragment_currencies.*
import rcmkt.revoluttest.R
import rcmkt.revoluttest.di.DI
import rcmkt.revoluttest.extension.observe
import rcmkt.revoluttest.extension.obtainViewModel
import javax.inject.Inject

class CurrenciesFragment : Fragment() {

    companion object {
        fun newInstance() = CurrenciesFragment()
    }

    @Inject
    lateinit var viewModelFactory: CurrenciesViewModelFactory

    private lateinit var viewModel: CurrenciesViewModel
    private val listAdapter = GroupAdapter<ViewHolder>()

    override fun onAttach(context: Context?) {
        DI.app.provideComponent().inject(this)
        super.onAttach(context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_currencies, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with(recycler) {
            adapter = listAdapter
            (itemAnimator as DefaultItemAnimator).supportsChangeAnimations = false
            recycledViewPool.setMaxRecycledViews(R.layout.item_currency, 10) // for better scrolling performance
            setHasFixedSize(true)
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = obtainViewModel(viewModelFactory)

        observe(viewModel.currencies, listAdapter::update)
        observe(viewModel.scrollToTop) { shouldScroll ->
            if (shouldScroll) recycler.scrollToPosition(0)
        }
        observe(viewModel.isLoading) { isLoading ->
            if (isLoading) progressView.show() else progressView.hide()
        }
    }
}
