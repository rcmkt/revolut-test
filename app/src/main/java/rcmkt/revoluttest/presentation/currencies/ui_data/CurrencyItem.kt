package rcmkt.revoluttest.presentation.currencies.ui_data

import android.annotation.SuppressLint
import com.xwray.groupie.kotlinandroidextensions.Item
import com.xwray.groupie.kotlinandroidextensions.ViewHolder
import kotlinx.android.synthetic.main.item_currency.view.*
import rcmkt.revoluttest.R
import rcmkt.revoluttest.extension.EMPTY
import rcmkt.revoluttest.extension.showKeyboardWithDelay
import rcmkt.revoluttest.utils.CurrencyFormatter
import rcmkt.revoluttest.view.AmountInputListener

data class CurrencyItem(
    val code: String,
    val exchangeRate: Double,
    val currentExchangeSum: Double,
    val isSelectedCurrency: Boolean = false,
    val amountInputListener: AmountInputListener,
    val onClickListener: (Int, String, Double) -> Unit
) : Item() {

    @SuppressLint("SetTextI18n")
    override fun bind(viewHolder: ViewHolder, position: Int) {
        with(viewHolder.containerView) {
            currencyName.text = code

            with(inputAmount) {
                isEnabled = isSelectedCurrency

                if (isSelectedCurrency) {
                    val text = getDisplayableValue()
                    removeAmountListener()
                    requestFocus()
                    setAmount(text)
                    setAmountListener(amountInputListener)
                    showKeyboardWithDelay()
                } else {
                    removeAmountListener()
                    setAmount(getDisplayableValue())
                }
            }

            setOnClickListener {
                if (!isSelectedCurrency) {
                    onClickListener.invoke(
                        position,
                        code,
                        inputAmount.getAmount()
                    )
                }
            }
        }
    }

    override fun unbind(holder: ViewHolder) {
        with(holder.containerView.inputAmount) {
            clearFocus()
            removeAmountListener()
        }
        super.unbind(holder)
    }

    override fun getLayout(): Int = R.layout.item_currency

    override fun getId(): Long = code.hashCode().toLong()

    private fun getDisplayableValue(): String {
        val amount = if (isSelectedCurrency) currentExchangeSum else exchangeRate * currentExchangeSum

        // The only reason of such condition is to display the hint '0' instead of '0.00' text
        return when (currentExchangeSum) {
            0.0 -> String.EMPTY
            else -> CurrencyFormatter.formatToString(amount)
        }
    }
}