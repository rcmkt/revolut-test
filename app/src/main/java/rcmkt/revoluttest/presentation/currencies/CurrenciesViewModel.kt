package rcmkt.revoluttest.presentation.currencies

import androidx.lifecycle.MutableLiveData
import com.xwray.groupie.kotlinandroidextensions.Item
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import io.reactivex.functions.BiFunction
import io.reactivex.subjects.BehaviorSubject
import rcmkt.domain.internal.SchedulersProvider
import rcmkt.domain.model.SelectedCurrency
import rcmkt.domain.usecase.CurrencyUseCase
import rcmkt.revoluttest.extension.schedulersIoToMain
import rcmkt.revoluttest.presentation.base.BaseViewModel
import rcmkt.revoluttest.presentation.currencies.ui_data.CurrencyItem
import rcmkt.revoluttest.view.AmountInputListener
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicBoolean

class CurrenciesViewModel(
    private val currencyUseCase: CurrencyUseCase,
    private val schedulersProvider: SchedulersProvider
) : BaseViewModel() {

    companion object {
        private const val DEFAULT_EXCHANGE_SUM = 0.0
        private const val DEFAULT_EXCHANGE_RATE = 1.0
        private const val DEFAULT_CURRENCY_CODE = "EUR"
        private const val DEBOUNCE_TIME = 300L
    }

    val currencies = MutableLiveData<MutableList<Item>>()
    val scrollToTop = MutableLiveData<Boolean>()
    val isLoading = MutableLiveData<Boolean>()

    private val exchangeSumSubject = BehaviorSubject.createDefault(DEFAULT_EXCHANGE_SUM)

    private val inputListener = object : AmountInputListener {
        override fun onAmountChanged(amount: Double) {
            if (amount == DEFAULT_EXCHANGE_SUM) {
                resetExchangeSum()
            } else if (amount != currentExchangeValue) {
                updateExchangeSum(amount)
            }
        }
    }

    private var currentExchangeValue = DEFAULT_EXCHANGE_SUM
    private var currentCurrencyCode = DEFAULT_CURRENCY_CODE

    private var currenciesDisposable: Disposable? = null
    private var shouldScrollToTop = AtomicBoolean(true)

    init {
        isLoading.value = true
        observeCurrenciesUpdates()
    }

    private fun observeCurrenciesUpdates() {
        currenciesDisposable?.dispose()
        currenciesDisposable = Observable
            .combineLatest<SelectedCurrency, Double, Pair<SelectedCurrency, Double>>(
                currencyUseCase.getCurrencies(currentCurrencyCode),
                exchangeSumSubject.debounce(DEBOUNCE_TIME, TimeUnit.MILLISECONDS),
                BiFunction { currency, exchangeValue ->
                    return@BiFunction Pair(currency, exchangeValue)
                }
            )
            .map { (currency, exchangeValue) -> toUiData(currency, exchangeValue) }
            .schedulersIoToMain(schedulersProvider)
            .doOnEach { isLoading.value = false }
            .subscribe({ currenciesItems ->
                currencies.value = currenciesItems
            }, {
                it.printStackTrace()
            })
            .autoDispose()
    }

    private fun toUiData(source: SelectedCurrency, currentExchangeValue: Double): MutableList<Item> {
        // Update existing list otherwise create a new one
        val items = currencies.value ?: ArrayList(source.rates.size + 1)

        if (items.isNotEmpty()) {
            items.forEachIndexed { index, item ->
                (item as? CurrencyItem)?.let { currencyItem ->
                    items[index] = currencyItem.copy(
                        exchangeRate = source.rates[currencyItem.code] ?: DEFAULT_EXCHANGE_RATE,
                        currentExchangeSum = currentExchangeValue
                    )
                }
            }
        } else {
            items.add(
                CurrencyItem(
                    code = source.code,
                    exchangeRate = DEFAULT_EXCHANGE_RATE,
                    isSelectedCurrency = true,
                    amountInputListener = inputListener,
                    currentExchangeSum = currentExchangeValue,
                    onClickListener = ::changeSelectedCurrency
                )
            )
            items.addAll(
                source.rates.map { currency ->
                    CurrencyItem(
                        code = currency.key,
                        exchangeRate = currency.value,
                        amountInputListener = inputListener,
                        currentExchangeSum = currentExchangeValue,
                        onClickListener = ::changeSelectedCurrency
                    )
                }
            )
        }
        return items
    }

    private fun changeSelectedCurrency(itemPosition: Int, code: String, newSum: Double) {
        currentCurrencyCode = code
        replaceWithTopItem(itemPosition, newSum)
        updateExchangeSum(newSum)
        observeCurrenciesUpdates()
    }

    private fun updateExchangeSum(sum: Double) {
        currentExchangeValue = sum
        exchangeSumSubject.onNext(sum)
    }

    private fun resetExchangeSum() {
        currentExchangeValue = DEFAULT_EXCHANGE_SUM
        exchangeSumSubject.onNext(DEFAULT_EXCHANGE_SUM)
    }

    private fun replaceWithTopItem(itemPosition: Int, newSum: Double) {
        currencies.value?.let { list ->
            list[0] = (list[0] as CurrencyItem).copy(
                isSelectedCurrency = false
            )

            val swapItem = (list[itemPosition] as CurrencyItem).copy(
                isSelectedCurrency = true,
                currentExchangeSum = newSum
            )
            list.removeAt(itemPosition)
            list.add(0, swapItem)
            currencies.value = list
        }
        scrollToTop.value = shouldScrollToTop.getAndSet(true)
    }
}
