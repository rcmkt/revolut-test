package rcmkt.revoluttest.presentation.currencies

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import rcmkt.domain.internal.SchedulersProvider
import rcmkt.domain.usecase.CurrencyUseCase
import javax.inject.Inject

class CurrenciesViewModelFactory @Inject constructor(
    private val currencyUseCase: CurrencyUseCase,
    private val schedulersProvider: SchedulersProvider
) : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return CurrenciesViewModel(currencyUseCase, schedulersProvider) as T
    }
}