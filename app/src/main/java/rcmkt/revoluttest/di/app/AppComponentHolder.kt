package rcmkt.revoluttest.di.app

import android.content.Context
import rcmkt.revoluttest.di.base.RootComponentHolder

class AppComponentHolder(private val context: Context) : RootComponentHolder<AppComponent>() {
    override fun provideInternal(): AppComponent {
        return DaggerAppComponent
                .builder()
                .appContext(context)
                .build()
    }
}

