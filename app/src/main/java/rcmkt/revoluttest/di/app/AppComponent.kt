package rcmkt.revoluttest.di.app

import android.content.Context
import dagger.BindsInstance
import dagger.Component
import rcmkt.revoluttest.MainActivity
import rcmkt.revoluttest.presentation.currencies.CurrenciesFragment
import javax.inject.Singleton

@Singleton
@Component(
        modules = [
            AppModule::class,
            NetworkModule::class
        ]
)
interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun appContext(appContext: Context): Builder

        fun build(): AppComponent
    }

    fun inject(entry: MainActivity)
    fun inject(entry: CurrenciesFragment)
}