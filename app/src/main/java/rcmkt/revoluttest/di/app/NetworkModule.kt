package rcmkt.revoluttest.di.app

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import rcmkt.data.network.ApiFactory
import rcmkt.data.network.HttpClientFactory
import rcmkt.data.network.RetrofitApi
import javax.inject.Singleton

@Module
class NetworkModule {

    @Provides
    @Singleton
    fun httpClient() = HttpClientFactory().buildClientWith()

    @Provides
    @Singleton
    fun gson() = GsonBuilder().create()

    @Provides
    @Singleton
    fun api(gson: Gson, httpClient: OkHttpClient) = ApiFactory(gson).createApi(RetrofitApi::class.java, httpClient)
}