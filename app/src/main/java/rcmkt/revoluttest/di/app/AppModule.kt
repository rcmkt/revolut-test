package rcmkt.revoluttest.di.app

import dagger.Module
import dagger.Provides
import rcmkt.data.network.RetrofitApi
import rcmkt.data.repository.CurrencyRepositoryImpl
import rcmkt.domain.internal.SchedulersProvider
import rcmkt.domain.repository.CurrencyRepository
import rcmkt.domain.usecase.CurrencyUseCase
import rcmkt.revoluttest.internal.SchedulersProviderImpl
import rcmkt.revoluttest.presentation.currencies.CurrenciesViewModelFactory
import javax.inject.Singleton

@Module
class AppModule {

    @Provides
    @Singleton
    fun provideSchedulersProvider(): SchedulersProvider {
        return SchedulersProviderImpl()
    }

    @Provides
    @Singleton
    fun provideCurrencyRepository(retrofitApi: RetrofitApi): CurrencyRepository {
        return CurrencyRepositoryImpl(retrofitApi)
    }

    @Provides
    @Singleton
    fun provideCurrencyViewModelFactory(
        currencyUseCase: CurrencyUseCase,
        schedulersProvider: SchedulersProvider
    ): CurrenciesViewModelFactory {
        return CurrenciesViewModelFactory(currencyUseCase, schedulersProvider)
    }
}