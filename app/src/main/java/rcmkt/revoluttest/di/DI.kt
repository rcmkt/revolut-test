package rcmkt.revoluttest.di

import android.annotation.SuppressLint
import android.content.Context
import rcmkt.revoluttest.di.app.AppComponentHolder

@SuppressLint("StaticFieldLeak")
object DI {

    val app: AppComponentHolder by lazy {
        AppComponentHolder(context)
    }

    private lateinit var context: Context

    fun init(context: Context) {
        DI.context = context
    }
}