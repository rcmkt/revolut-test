package rcmkt.revoluttest.di.base

interface ComponentHolder<out Component> {
    fun provideComponent(): Component

    fun onDependencyReleased()
}