package rcmkt.data.repository

import io.reactivex.Observable
import rcmkt.data.converter.CurrencyConverter
import rcmkt.data.network.RetrofitApi
import rcmkt.domain.model.SelectedCurrency
import rcmkt.domain.repository.CurrencyRepository
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CurrencyRepositoryImpl @Inject constructor(
    private val api: RetrofitApi
) : CurrencyRepository {

    override fun getCurrencies(baseCurrencyCode: String): Observable<SelectedCurrency> {
        return api.getCurrencies(baseCurrencyCode)
            .map(CurrencyConverter::toDomain)
    }
}