package rcmkt.data.converter

import rcmkt.data.model.SelectedCurrencyDto
import rcmkt.domain.model.SelectedCurrency

object CurrencyConverter {

    fun toDomain(source: SelectedCurrencyDto): SelectedCurrency {
        return SelectedCurrency(
            code = requireNotNull(source.code),
            lastDate = requireNotNull(source.lastDate),
            rates = requireNotNull(source.rates)
        )
    }
}