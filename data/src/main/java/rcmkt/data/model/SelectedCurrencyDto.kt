package rcmkt.data.model

import com.google.gson.annotations.SerializedName

data class SelectedCurrencyDto(

    @SerializedName("base")
    val code: String?,

    @SerializedName("date")
    val lastDate: String?,

    @SerializedName("rates")
    val rates: Map<String, Double>?
)