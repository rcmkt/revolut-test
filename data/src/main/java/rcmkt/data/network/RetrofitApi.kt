package rcmkt.data.network

import io.reactivex.Observable
import rcmkt.data.model.SelectedCurrencyDto
import retrofit2.http.GET
import retrofit2.http.Query

interface RetrofitApi {

    @GET("latest")
    fun getCurrencies(@Query("base") baseCurrencyCode: String): Observable<SelectedCurrencyDto>
}