package rcmkt.data.network

import android.annotation.SuppressLint
import android.content.Context
import com.ihsanbal.logging.Level
import com.ihsanbal.logging.LoggingInterceptor
import okhttp3.CertificatePinner
import okhttp3.OkHttpClient
import rcmkt.data.BuildConfig
import java.util.concurrent.TimeUnit.MILLISECONDS as MILLIS

class HttpClientFactory {

    companion object {
        private const val CONNECT_TIMEOUT_MILLIS = 5_000L
        private const val READ_TIMEOUT_MILLIS = 30_000L
        private const val WRITE_TIMEOUT_MILLIS = 10_000L
    }

    fun buildClientWith(): OkHttpClient {
        with(OkHttpClient.Builder()) {
            connectTimeout(CONNECT_TIMEOUT_MILLIS, MILLIS)
            readTimeout(READ_TIMEOUT_MILLIS, MILLIS)
            writeTimeout(WRITE_TIMEOUT_MILLIS, MILLIS)

            interceptors().add(getLoggingInterceptor())

            return build()
        }
    }

    @SuppressLint("LogNotTimber")
    internal fun getLoggingInterceptor(): LoggingInterceptor {
        return LoggingInterceptor.Builder()
            .loggable(BuildConfig.DEBUG)
            .setLevel(Level.BASIC)
            .request("Request")
            .response("Response")
            .build()
    }
}