package rcmkt.data.network

import com.google.gson.Gson
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class ApiFactory(private val gson: Gson) {

    fun <T> createApi(clazz: Class<T>, client: OkHttpClient): T {
        val builder = Retrofit.Builder()
        return builder.baseUrl(ServerUrl.getAddress())
            .client(client)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
            .create(clazz)
    }
}