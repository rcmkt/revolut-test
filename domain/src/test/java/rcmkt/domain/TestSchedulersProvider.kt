package rcmkt.domain

import io.reactivex.Scheduler
import io.reactivex.schedulers.TestScheduler
import rcmkt.domain.internal.SchedulersProvider

class TestSchedulersProvider : SchedulersProvider {

    private val testScheduler = TestScheduler()

    override fun io(): Scheduler = testScheduler

    override fun computation(): Scheduler = testScheduler

    override fun mainThread(): Scheduler = testScheduler
}