package rcmkt.domain.usecase

import com.nhaarman.mockitokotlin2.mock
import io.reactivex.Observable
import io.reactivex.observers.TestObserver
import io.reactivex.schedulers.TestScheduler
import org.junit.Test
import rcmkt.domain.TestSchedulersProvider
import rcmkt.domain.model.SelectedCurrency
import rcmkt.domain.repository.CurrencyRepository
import java.util.concurrent.TimeUnit

class CurrencyUseCaseTest {

    private val schedulersProvider = TestSchedulersProvider()

    private val mockResponse = SelectedCurrency(
        code = "EUR",
        lastDate = "",
        rates = emptyMap()
    )

    @Test
    fun `when request is successful then expect no errors`() {

        //given
        val currencyCode = "EUR"
        val mockRepository = mock<CurrencyRepository> {
            on { getCurrencies(currencyCode) }.thenReturn(Observable.just(mockResponse))
        }
        val useCase = CurrencyUseCase(mockRepository, schedulersProvider)
        val testObserver = TestObserver.create<SelectedCurrency>()

        //when
        useCase.getCurrencies(currencyCode)
            .observeOn(schedulersProvider.io())
            .subscribe(testObserver)

        //then
        testObserver.assertNoErrors()
    }

    @Test
    fun `when request is successful then retry it after 1 second`() {

        //given
        val currencyCode = "EUR"
        val mockRepository = mock<CurrencyRepository> {
            on { getCurrencies(currencyCode) }.thenReturn(Observable.just(mockResponse))
        }
        val useCase = CurrencyUseCase(mockRepository, schedulersProvider)
        val testObserver = TestObserver.create<SelectedCurrency>()

        //when
        useCase.getCurrencies(currencyCode)
            .subscribeOn(schedulersProvider.io())
            .subscribe(testObserver)

        (schedulersProvider.io() as TestScheduler).advanceTimeBy(1, TimeUnit.SECONDS)

        //then
        testObserver.assertValueCount(2)
    }

    @Test
    fun `when request is fail then retry it and subscription is alive with result`() {
        //given
        val currencyCode = "EUR"
        val mockRepository = mock<CurrencyRepository> {
            on { getCurrencies(currencyCode) }.thenReturn(Observable.error(RuntimeException()))
            on { getCurrencies(currencyCode) }.thenReturn(Observable.just(mockResponse))
        }
        val useCase = CurrencyUseCase(mockRepository, schedulersProvider)
        val testObserver = TestObserver.create<SelectedCurrency>()

        //when
        useCase.getCurrencies(currencyCode)
            .subscribeOn(schedulersProvider.io())
            .subscribe(testObserver)

        (schedulersProvider.io() as TestScheduler).advanceTimeBy(1, TimeUnit.SECONDS)

        //then
        testObserver.assertValueCount(2) // because of repeatWhen operator
        testObserver.assertNoErrors()
        testObserver.assertNotTerminated()
        testObserver.assertNotComplete()
    }


}