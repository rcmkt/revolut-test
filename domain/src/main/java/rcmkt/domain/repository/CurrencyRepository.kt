package rcmkt.domain.repository

import io.reactivex.Observable
import rcmkt.domain.model.SelectedCurrency

interface CurrencyRepository {
    fun getCurrencies(baseCurrencyCode: String): Observable<SelectedCurrency>
}