package rcmkt.domain.model

data class SelectedCurrency(
    val code: String,
    val lastDate: String,
    val rates: Map<String, Double>
)