package rcmkt.domain.usecase

import io.reactivex.Observable
import rcmkt.domain.internal.SchedulersProvider
import rcmkt.domain.model.SelectedCurrency
import rcmkt.domain.repository.CurrencyRepository
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class CurrencyUseCase @Inject constructor(
    private val currencyRepository: CurrencyRepository,
    private val schedulersProvider: SchedulersProvider
) {

    companion object {
        private const val PERIOD_TIME_TO_REPEAT = 1L
    }

    fun getCurrencies(baseCurrencyCode: String): Observable<SelectedCurrency> {
        return currencyRepository.getCurrencies(baseCurrencyCode)
            .repeatWhen { it.delay(PERIOD_TIME_TO_REPEAT, TimeUnit.SECONDS, schedulersProvider.computation()) }
            .retryWhen { it.flatMap { Observable.timer(PERIOD_TIME_TO_REPEAT, TimeUnit.SECONDS, schedulersProvider.computation()) } }
    }
}